/*
	Create an arrow function called postCourse which allows us to add a new object into the array. It should receive data: id, name, description, price, isActive.
		-push()

	Add the new course into the array and show the following alert:
		"You have created <nameOfCourse>. The price is <priceOfCourse>."
		-literal `${}`

	Create an arrow function which allows us to find a particular course providing the course id and return the details of the found course.
		- use find()

	Create an arrow function called deleteCourse which can delete the last course object in the array.
		- pop()
 */

/*Pushing Instructions:

	Go to Gitlab:
		-in your zuitt-projects folder and access b131 folder.
		-inside your b131 folder create a new folder/subgroup: s20
		-inside s20, create a new project/repo called activity
		-untick the readme option
		-copy the git url from the clone button of your activity repo.

	Go to Gitbash:
		-go to your b131/s20 folder and access activity folder
		-initialize activity folder as a local repo: git init
		-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
		-add your updates to be committed: git add .
		-commit your changes to be pushed: git commit -m "includes JSON activity 1"
		-push your updates to your online repo: git push origin master

	Go to Boodle:
		-copy the url of the home page for your s20/activity repo (URL on browser not the URL from clone button) and link it to boodle:

		WD078-20 | Javascript - Introduction to JSON*/

let courseList=[];

const postCourse = (id,name,description,price,isActive) =>{
	courseList.push({
		'id':id,
		'name':name,
		'description':description,
		'price':price,
		'isActive':isActive
	});

	alert(`You have created ${name}. The price is ${price}.`);
}
postCourse("001","Python Bootcamp Beginner to Advance","Study Basic Python to Advance","$10","Available");

const courseFinder = (find) =>{
	let courseFound = courseList.find((find)=>{
	alert(`You have found the  Id number ${find.id}: course ${find.name}.`)
	});
}
courseFinder('001')


const courseUnenrolled = () =>{
	courseList.pop();
	alert(`You Have Successfully Unenrolled for course`)
}
courseUnenrolled();